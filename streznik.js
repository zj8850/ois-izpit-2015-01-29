var express = require('express'), path = require('path'), fs = require('fs');

var app = express();
app.use(express.static(__dirname + '/public'));

var podatkiSpomin = ["admin/nimda", "gost/gost"];


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Zunanja avtentikacija)
 */
app.get('/api/prijava', function(req, res) {
	// ...
	var uporabniskoIme = req.query.uporabniskoIme;
	var geslo = req.query.geslo;
	var uspesno = preveriSpomin(uporabniskoIme,geslo) || preveriDatotekaStreznik(uporabniskoIme,geslo);
	var napaka;
	if(uporabniskoIme == "" || geslo == "") napaka = "Napačna zahteva!"
	else if (!uspesno) napaka = "Avtentikacija ni uspešna!"
	res.send({status: uspesno, napaka: napaka});
	// ...
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Prijava uporabnika v sistem)
 */
app.get('/prijava', function(req, res) {
	// ...
	var uporabniskoIme = req.query.uporabniskoIme;
	var geslo = req.query.geslo;
	var uspesno = preveriSpomin(uporabniskoIme,geslo) || preveriDatotekaStreznik(uporabniskoIme,geslo);
	var odgovor = "<html><title>";
	if (uspesno == true) odgovor += "Uspesno";
	else odgovor += "Napaka";
	odgovor += "</title><body><p>Uporabnik <b>"+ uporabniskoIme +"</b>";
	if (uspesno == true) odgovor += " uspešno prijavljen v sistem!";
	else odgovor += " nima pravice prijave v sistem!";
	odgovor += "</p></body></html>";
	
	
	res.send(odgovor);
	// ...
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var podatkiSpomin = ["admin/nimda", "gost/gost"];


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (branje datoteka na strani strežnika)
 */
var podatkiDatotekaStreznik = JSON.parse(fs.readFileSync(__dirname + "/public/podatki/uporabniki_streznik.json").toString());


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti
 */
function preveriSpomin(uporabniskoIme, geslo) {
	for (i in podatkiSpomin){
					username = podatkiSpomin[i].split("/")[0];
					password = podatkiSpomin[i].split("/")[1];
					if(username == uporabniskoIme && password == geslo) return true;
				}
				return false;
}


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti
 */
function preveriDatotekaStreznik(uporabniskoIme, geslo) {
	for (i in podatkiDatotekaStreznik){
		username = podatkiDatotekaStreznik[i]["uporabnik"];
		password = podatkiDatotekaStreznik[i]["geslo"];
		if ( username == uporabniskoIme && password == geslo) return true;
	}
	return false;
	// ...
}